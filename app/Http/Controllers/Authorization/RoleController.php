<?php

namespace App\Http\Controllers\Authorization;

use Illuminate\Http\Request;
use App\Models\Authorization\Role;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        // $data = EmployeeDB::where('nip', 'ID011407441')->first();
        $data = Role::all();

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2','unique:roles,name']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user_id = $this->guard()->user()->id;

        try {
            $data = [];
            $data['name'] = $request->name;
            $data['guard_name'] = 'api';
            $data['description'] = $request->description;
            $data['created_at'] = now();
            $data['user_id'] = $user_id;
            $data['updated_at'] = now();

            $insert = Role::insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert,
                'data' => $data
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = Role::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
                }
            return ResponseBuilder::result($status, $message, $data, $response_code);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2','unique:roles,name']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user_id = $this->guard()->user()->id;

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_CREATED;
        $data = Role::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
            }

            $data = [];
            $data['name'] = $request->name;
            $data['guard_name'] = 'api';
            $data['description'] = $request->description;
            $data['created_at'] = now();
            $data['user_id'] = $user_id;
            $data['updated_at'] = now();


            $update = DB::table('roles')
            ->where('id','=', $id)
            ->update($data);

            $response = [
                'message'=>'Data successfully update.',
                'status'=> $update,
                'data' => $data
            ];


            return ResponseBuilder::result($status, $message, $data, $response_code);


        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $status = true;
        $message  = "Data berhasil di hapus";
        $response_code = Response::HTTP_OK;
        $data = Role::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
                }

            $data->delete();

            return ResponseBuilder::result($status, $message, $data, $response_code);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    public function search() {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;

        $data = QueryBuilder::for(Role::class)
        ->allowedFilters(['name','description'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }


}
