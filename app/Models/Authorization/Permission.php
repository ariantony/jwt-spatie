<?php

namespace App\Models\Authorization;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    public $table = 'permissions';
    protected $fillable = [
        'id',
        'name',
        'guard_name',
        'user_id',
        'description',
        'created_at',
        'updated_at'
    ];

    protected $guarded = [

    ];

    protected $hidden = [
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
}
