<?php

namespace App\Models\Authorization;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    //protected $connection = 'mysql_api';

    public $table = 'roles';

    protected $fillable = [
        'id',
        'name',
        'guard_name',
        'description',
        'created_at',
        'updated_at'
    ];

    protected $guarded = [

    ];

    protected $hidden = [
    ];


    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

}
