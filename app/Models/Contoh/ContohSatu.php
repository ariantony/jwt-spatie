<?php

namespace App\Models\Contoh;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContohSatu extends Model
{
    use HasFactory;
}
