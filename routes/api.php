<?php

use App\Http\Controllers\Authorization\PermissionController;
use App\Http\Controllers\Authorization\RoleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Contoh\ContohSatuController;
use App\Models\Authorization\Role;

/*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
*/

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'auth',
    ],
    function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::get('profile', 'AuthController@profile');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('users', 'AuthController@users');
    }
);

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
    ],
    function ($router) {
        Route::resource('todos', 'TodoController');

        Route::get('roles',[RoleController::class, 'index']);


    }
);

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'authorization',
    ],
    function ($router) {

        Route::get('roles',[RoleController::class, 'index']);
        Route::post('roles',[RoleController::class, 'store']);
        Route::get('role/{id}',[RoleController::class, 'show']);
        Route::put('role/{id}',[RoleController::class, 'update']);
        Route::delete('role/{id}',[RoleController::class, 'destroy']);
        Route::get('roles/search',[RoleController::class, 'search']);


        Route::get('permissions',[PermissionController::class, 'index']);
        Route::post('permissions',[PermissionController::class, 'store']);
        Route::get('permission/{id}',[PermissionController::class, 'show']);
        Route::put('permission/{id}',[PermissionController::class, 'update']);
        Route::delete('permission/{id}',[PermissionController::class, 'destroy']);

    }
);




Route::get('contohsatu',[ContohSatuController::class, 'index']);
